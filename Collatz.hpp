// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// cycle_length
// ------------
/**
* calculate cycle length for the given long
* @param a unsigned long
* @return integer
*/
int cycle_length (unsigned long n);

// ------------
// max_cycle_length
// ------------
/**
* calculate max cycle length for the given range
* @param a pair of ints
* @return integer
*/
int max_cycle_length (const pair<int, int>& p);

// ------------
// cache_mcl
// ------------

/**
* calculate max cycle length for the given range using cache mostly
* @param a pair of ints
* @return integer
*/
int cache_mcl (const pair<int, int>& p);

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
